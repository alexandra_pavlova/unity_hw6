using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class TimerScript : MonoBehaviour
{
    [SerializeField] public Text startingTimeText;
    private float _momentTime = 60f;
    public float startingTime = 60f;

    void Start()
    {
        _momentTime = startingTime;
    }
    
    void Update()
    {
        if (StateMachine.Instance.gameScreen.activeSelf)
        {
            TimeDecrease();
            CheckForWinLose();
        }
    }

    public void TimeDecrease()
    {
        if (_momentTime >= 1)
        {
            _momentTime -= Time.deltaTime;
            print(_momentTime);
            if (StateMachine.Instance.gameScreen.activeSelf)
            {
                startingTimeText.text = Mathf.Round(_momentTime).ToString();
            }
        }
    }

    public void CheckForWinLose()
    {
        if ((StateMachine.Instance.gameScreen.activeSelf) 
            && (_momentTime < 1) 
            && (StateMachine.Instance.winScreen.activeSelf == false))
        {
            Tools.Instance.RestartPin();
            StateMachine.Instance.ChangeState(StateMachine.Instance.loseScreen); // работает
        }
        else if ((StateMachine.Instance.gameScreen.activeSelf) &&
                 (Tools.Instance.pin1Value == Tools.Instance.pin2Value) &&
                 (Tools.Instance.pin1Value == Tools.Instance.pin3Value))
        {
            Tools.Instance.RestartPin();
            StateMachine.Instance.ChangeState(StateMachine.Instance.winScreen); // работает
        }
    }

    public void OnRestartButtonClick()
    {
        if ((StateMachine.Instance.winScreen.activeSelf) &&  
            (StateMachine.Instance.gameScreen.activeSelf == false)) 
        {
            //startingTimeText.text = startingTime.ToString();
            _momentTime = startingTime;
            print(StateMachine.Instance.gameScreen);
            StateMachine.Instance.ChangeState(StateMachine.Instance.gameScreen);
        }
        
        
        else if ((StateMachine.Instance.loseScreen.activeSelf) &&
                 (StateMachine.Instance.gameScreen.activeSelf == false))
        {
            _momentTime = startingTime;
            startingTimeText.text = startingTime.ToString();
            StateMachine.Instance.ChangeState(StateMachine.Instance.gameScreen);
        }
    }
    
}





using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Tools : MonoBehaviour
{
    public static Tools Instance { get; private set; }
    
    [SerializeField] public Text pin1Text;
    [SerializeField] public Text pin2Text;
    [SerializeField] public Text pin3Text;
    
    public float pin1Value = 0f;
    public float pin2Value = 0f;
    public float pin3Value = 0f;
    
    private float pin1ValueDef = 0f;
    private float pin2ValueDef = 0f;
    private float pin3ValueDef = 0f;
    
    [SerializeField] public Text hammerValueText;
    public float hammerValue1 = 0f;
    public float hammerValue2 = 0f;
    public float hammerValue3 = 0f;
    
    [SerializeField] public Text drillValueText;
    public float drillValue1 = 0f;
    public float drillValue2 = 0f;
    public float drillValue3 = 0f;
    
    [SerializeField] public Text masterKeyValueText;
    public float masterKeyValue1 = 0f;
    public float masterKeyValue2 = 0f;
    public float masterKeyValue3 = 0f;

    // Start is called before the first frame update
    void Start()
    {
        Instance = this;

        pin1ValueDef = pin1Value;
        pin2ValueDef = pin2Value;
        pin3ValueDef = pin3Value;
        
        hammerValueText.text = "Молоток\n" + $"({hammerValue1.ToString()} {hammerValue2.ToString()} {hammerValue3.ToString()})";
        drillValueText.text = "Дрель\n" + $"({drillValue1.ToString()} {drillValue2.ToString()} {drillValue3.ToString()})";
        masterKeyValueText.text = "Отмычка\n" + $"({masterKeyValue1.ToString()} {masterKeyValue2.ToString()} {masterKeyValue3.ToString()})";
    }

    private void Update()
    {
        pin1Text.text = pin1Value.ToString();
        pin2Text.text = pin2Value.ToString();
        pin3Text.text = pin3Value.ToString();
    }

    public void RestartPin()
    {
        pin1Value = pin1ValueDef;
        pin2Value = pin2ValueDef;
        pin3Value = pin3ValueDef;
    }
    
    public void OnClickHammer()
    {
        if (((pin1Value + hammerValue1) <= 10) && ((pin1Value + hammerValue1) >= 0))
        {
            if (((pin2Value + hammerValue2) <= 10) && ((pin2Value + hammerValue2) >= 0))
            {
                if (((pin3Value + hammerValue3) <= 10) && ((pin3Value + hammerValue3) >= 0))
                {
                    pin1Value = pin1Value + hammerValue1;
                    pin2Value = pin2Value + hammerValue2;
                    pin3Value = pin3Value + hammerValue3;
                }
            }
        }
    }
    
    public void OnClickDrill()
    {
        if (((pin1Value + drillValue1) <= 10) && ((pin1Value + drillValue1) >= 0))
        {
            if (((pin2Value + drillValue2) <= 10) && ((pin2Value + drillValue2) >= 0))
            {
                if (((pin3Value + drillValue3) <= 10) && ((pin3Value + drillValue3) >= 0))
                {
                    pin1Value = pin1Value + drillValue1;
                    pin2Value = pin2Value + drillValue2;
                    pin3Value = pin3Value + drillValue3;
                }
            }
        }
        
    }
    
    public void OnClickMasterKey()
    {
        if (((pin1Value + masterKeyValue1) <= 10) && ((pin1Value + masterKeyValue1) >= 0))
        {
            if (((pin2Value + masterKeyValue2) <= 10) && ((pin2Value + masterKeyValue2) >= 0))
            {
                if (((pin3Value + masterKeyValue3) <= 10) && ((pin3Value + masterKeyValue3) >= 0))
                {
                    pin1Value = pin1Value + masterKeyValue1;
                    pin2Value = pin2Value + masterKeyValue2;
                    pin3Value = pin3Value + masterKeyValue3;
                }
            }
        }
        
    }
}

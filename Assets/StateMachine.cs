using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMachine : MonoBehaviour
{
    public static StateMachine Instance { get; private set; }
    private GameObject _currentScreen;
    
    [SerializeField] public GameObject startScreen;
    [SerializeField] public GameObject gameScreen;
    [SerializeField] public GameObject winScreen;
    [SerializeField] public GameObject loseScreen;

    private void Start()
    {
        Instance = this;
        //gameScreen.SetActive(true);
        //_currentScreen = gameScreen;
        startScreen.SetActive(true);
        _currentScreen = startScreen;
        
    }
    
    public void ChangeState(GameObject state)
    {
        print(_currentScreen);
        if (_currentScreen != null)
        {
            _currentScreen.SetActive(false);
            state.SetActive(true);
            _currentScreen = state;
        }
    }
}


